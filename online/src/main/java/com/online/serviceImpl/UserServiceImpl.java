package com.online.serviceImpl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


import com.online.dao.UserDAO;
import com.online.dto.UserDTO;
import com.online.model.User;
import com.online.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Resource(name = "userDao")
	private UserDAO userDao;

	@Override
	
	public UserDTO login(UserDTO userdto) {
		User user = new User();
		user.setUserName(userdto.getUserName());
		user.setPassword(userdto.getPassword());
		userDao.createUser(user);

		return userdto;

	}

}
